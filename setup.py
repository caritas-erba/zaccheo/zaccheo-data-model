import setuptools

from zaccheo_data_model.__version__ import __version__

setuptools.setup(
    name="zaccheo-data-model",
    version=__version__,
    author="Mattia Tantardini <mattia.tantardini@gmail.com>",
    author_email="",
    description="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8,<4.0",
)
